<?php
include_once('lib/application.php');

if (strtoupper($_SERVER['REQUEST_METHOD']) == 'POST') {
    //var_dump($_POST);
    if (!array_key_exists('myform', $_SESSION)) {
        $_SESSION['myform'] = array();
    }
    
    $_SESSION['myform'] []= $_POST;
    header('location:index.php');
}
    
else{
    header('location:create.html');
}
