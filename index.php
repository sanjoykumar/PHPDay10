<?php
include_once('lib/application.php');
$myform = $_SESSION['myform'];
?>


<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title><?php echo PAGE_TITLE; ?></title>
    </head>
    <body>
        <p>
            <a href="create.html">Click here</a> to add new email.
        <div>
            <?php
            if (array_key_exists('message', $_SESSION) && !empty($_SESSION['message'])) {
                echo $_SESSION['message'];
            }
            ?>
        </div>
    </p>
    <table border="1">
        <tr>
            <th>Sl</th>
            <th>Last Name</th>
            <th>First Name</th>
            <th>Middle Name</th>
            <th>Date of Birth</th>
            <th>Gender</th>
            <th>Social Security Number</th>
            <th>Facility Number</th>
        </tr>

        <?php
        $counter = 1;
        foreach ($_SESSION['myform'] as $value) {
            ?>
            <tr>
                <td><?php echo $counter++; ?></td>
                <td><?php echo $value['lastname']; ?></td>
                <td><?php echo $value['firstname']; ?></td>
                <td><?php echo $value['middlename']; ?></td>
                <td><?php echo $value['dob']; ?></td>
                <td><?php echo $value['gender']; ?></td>
                <td><?php echo $value['scn']; ?></td>
                <td><?php echo $value['facilityname']; ?></td>
            </tr>

        <?php } ?>


    </table>



    <h3>Decedent of Hispanic Origin</h3>

    <table border="1">
        <tr>
            <th>Sl</th>
            <th>Origin</th>

        </tr>

        <?php
        $counter = 1;



        foreach ($_SESSION['myform'] as $value) {
            ?>
            <tr>
                <td><?php echo $counter++; ?></td>
                <td><?php
                    if ($value['origin'] == 'Other') {
                        echo $value['originother'];
                    } else {
                        echo $value['origin'];
                    }
                    ?></td>


            </tr>

<?php } ?>

    </table>		



    <h3>Decedent's Race</h3>

    <table border="1">
        <tr>
            <th>Sl</th>
            <th>Race</th>

        </tr>

<?php
$counter = 1;



foreach ($_SESSION['myform'] as $value) {
    ?>
            <tr>
                <td><?php echo $counter++; ?></td>
                <td>
    <?php
    if ($value['race'] == 'Other') {
        echo $value['racetext'];
    } else {
        echo $value['race'];
    }
    ?>
                </td>


            </tr>

<?php } ?>

    </table>		



</body>
</html>
